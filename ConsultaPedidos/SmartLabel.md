# Introdução
A API para retornar dados da etiqueta


### Solicitação GET
> /Etiqueta/hering/{cep}

# Parâmetros
|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
CEP|CEP|sim|String

## Etiqueta

NroPedido é importante para que nosso sorter possa ler a etiqueta, ele deve está dentro de uma DataMatrix.

<img src="imagem/SmartLabel.png" width="600px" height="418px"/>

## Resposta
|Campo|Descricao|            
|----------------|---------------|
dado_1|Endereçamento
dado_2|Setup Operação
dado_3|Tipo Operação
dado_4|Regionalização

```JS
{
    "status": true,
    "situacao": "Sucesso",
    "dados": {
        "dado_1": "G31",
        "dado_2": 3,
        "dado_3": "JBQ",
        "dado_4": "0436"
    }
}
```
