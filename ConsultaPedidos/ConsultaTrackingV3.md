# Introdução
A API para consultar  pedidos em massa


### Solicitação POST
> /Carriers/TrackingV3

# Parâmetros
|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
PedidoCliente|Chave identificadora do pedido|Não|Array
ChaveNota|Chave da nota|Não|Array

### Formato de json solicitado

No máximo 500 pedidos por requisição
```JS
{
   "PedidoCliente":[
    "41529046",
    "41349950",
    "41544632",
    "41544456"
   ]
}
```

```JS
{
   "ChaveNota":[
    "9999999999999999999999999999999999999999999",
    "9999999999999999999999999999999999999999999",
    "9999999999999999999999999999999999999999999",
    "9999999999999999999999999999999999999999999"
   ]
}
```

### Códigos de erro 

Erros de sintaxe ou inesperados (erro 500)
```JS
{
    "status": false,
    "data": {
        "situacao": "Erro interno"
    }
}
```

Erros de requisição (erro 400)
```JS
{
    "status": false,
    "data": {
        "situacao": "Nenhum dado informado"
    }
}
```

Json no formato incorreto (erro 500)
```JS
{
    "status": "erro",
    "erros": {
        "Json": [
            "Formato do json incorreto"
        ]
    }
}
```

Atingiu o limite de pedidos por consulta (erro 500)
```JS
{
    "status": "erro",
    "erros": {
        "Json": [
            "No máximo 500 pedidos por requisição"
        ]
    }
}
```

# Lista de Status
|idStatus|Status|           
|----------------|---------------|
20|  EM ROTA DE COLETA
22|  RECEBIDO NO CD
24|  CARGA NÃO COLETADA
31|  EM PREPARAÇÃO PARA TRANSPORTE
50|  EM TRANSFERÊNCIA PARA FILIAL
51|  RECEBIDO NA FILIAL DESTINO
52|  EM TRANSFERÊNCIA PARA FILIAL ORIGEM
100|  EM ROTA DE ENTREGA
101|  ENTREGA REALIZADA
102|  INSUCESSO DE ENTREGA
103|  PRONTO PARA REVERSA
104|  REVERSA REALIZADA
105|  INSUCESSO DE REVERSA
150|  EM DROPPOINT
151|  EDROP REALIZADO
196|  PENDENCIA DE REVERSA
197|  PENDÊNCIA DE COLETA
198|  PENDÊNCIA DE TRANSFERÊNCIA
199|  PENDÊNCIA DE DEVOLUÇÃO
200|  PENDÊNCIA DE ENTREGA
202|  REENTREGA AUTORIZADA
400|  DEVOLUÇÃO SOLICITADA
401|  RESOLVIDO PARA DEVOLUÇÃO
406|  EM ROTA DE DEVOLUÇÃO
411|  DEVOLUÇÃO REALIZADA
412|  INSUCESSO DE DEVOLUÇÃO
501|  AVARIA
502|  EXTRAVIO
503|  SINISTRO



## Resposta

```JS
{
    "PedidoCliente": "0012001029279",
    "idItemParceiro": 12072422,
    "NotaFiscal": "145745",
    "Cliente": "MUNDO INFANTIL",
    "Destinatario": "JAQUELINE SIMAO PEREIRA VIEIRA",
    "codigoRastreio": "145-0012001029279-14807165",
    "Url": "https://www.carriers.com.br/portal/localizador.php?l=145-0012001029279-14807165",
    "UrlProtocolo": "https://carriers.com.br/portaris/prot_entrega//2022/03/30/1309821/4000315105335.jpg",
    "Eventos": [
        {
            "Data": "27-01-2020 09:51:39",
            "Status": "ENTREGA REALIZADA",
            "idStatus": 101,
            "Descricao": "Entregue"
        },
        {
            "Data": "27-01-2020 08:57:16",
            "Status": "EM ROTA DE ENTREGA",
            "idStatus": 100,
            "Descricao": "Em rota de entrega"
        },
        {
            "Data": "27-01-2020 07:26:29",
            "Status": "RECEBIDO NA BASE",
            "idStatus": 51,
            "Descricao": "Na filial distribuidora"
        },
        {
            "Data": "26-01-2020 22:35:30",
            "Status": "EM TRANSFERÊNCIA PARA A BASE",
            "idStatus": 50,
            "Descricao": "Em transferência para filial Distribuidora"
        },
        {
            "Data": "24-01-2020 22:53:41",
            "Status": "COLETADO",
            "idStatus": 22,
            "Descricao": "Recebido no CD da transportadora"
        },
        {
            "Data": "24-01-2020 22:53:41",
            "Status": "ETIQUETADO SEPARAÇÃO",
            "idStatus": 31,
            "Descricao": "Em preparação para transporte"
        }
    ]
}
```
